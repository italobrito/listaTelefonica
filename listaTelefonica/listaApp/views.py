from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from .models import *

# CRUD

# Create

def contacts_new(request, template_name='contact_form.html'):
    form = ContactForm(request.POST or None, request.Files or None)

    dados = {'form': form}

    if form.is_valid():
        form.save()
        return redirect('contact_list')
    return render(request, template_name, dados)

# Read

def contacts_list(request, template_name='contact.html'):
    contacts = Contacts.objects.all()
    dados = {'contacts':contacts}
    return render(request, template_name, dados)

# Update

def contacts_update(request, id, template_name='contact_form.html'):
    contacts = get_object_or_404(Contacts, pk=id)
    form = ContactForm(request.POST or None, request.Files or None, instance=contacts)

    dados = {'form':form}
    if form.is_valid():
        form.save()
        return redirect('contacts_list')

    return render(request, template_name, dados)

# Delete

def contacts_delete(request, id, contacts_name='contacts_delete.html'):
    contacts = get_object_or_404(Contacts, pk=id)

    if request.method == 'POST':
        contacts.delete()
        return redirect('person_list')
