from django.db import models

class Contacts (models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    phone = models.CharField(max_length=12)
    email = models.EmailField(max_length=60)
    birth = models.DateTimeField()
    cpf = models.CharField(max_length=11)

    def __str__(self):
        return self.first_name