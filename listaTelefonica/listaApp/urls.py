from django.urls import path
from .views import *

urlpatterns = [
    path('list/', contacts_list, name='contact_list'),
    path('new/', contacts_new, name='contact_new'),
    path('update/<int:id>/', contacts_update, name='contact_update'),
    path('delete/<int:id>/', contacts_delete, name='contact_delete'),
]